# NgxPopoverDialog

Convert any component to a dialog.

## Import the module

Import the `NgxPopoverDialogModule` to your module.

Which imports from:

```typescript
import { NgxPopoverDialogModule } from "ngx-popover-dialog";
```

## Import styling
When you'd like to load default styling for the backdrop op you modo you'll have to import the following CSS.

```css
@import '~ngx-popover-dialog/css/prebuild.css';
```

## Make a component importable

Extend the Component you'd like to use as a modal from `Modal` and update the constructor to implement the `OverlayToken`.

Files can be imported from:

```typescript
import { Modal, OVERLAY_TOKEN, OverlayToken, ModalService } from "ngx-popover-dialog";
```

Don't forget to update your constructor, it would look something like this:

```typescript
    constructor(@Inject(OVERLAY_TOKEN) protected overlay: OverlayToken, protected modalService: ModalService) {
        super(overlay, modalService);
    }
```

## Implement the service

We can create a modal from anywhere.

We can simply import the `ModalService` from:

```typescript
import { ModalService } from "ngx-popover-dialog";
```

You can opening a modal by calling the `createModal` method.

For example:

```typescript
const modal = this.modalService.createModal(FancyModalComponent);
```

The first parameter will be the component you'd like to use and the second parameter will be the data you'd like to provide to the modal.

## Accessing data and closing the modal

### Closing the modal

Inside a modal component you can call the `this.close();` method to close the popover, this is possible due to the fact that the component inherits from `Modal`.

### Accessing the data

Data passed in the second parameter can be accessed inside the modal component by reading the following property:

```typescript
this.overlay.data;
```

## Callbacks

Right now a modal comes with two callbacks `onClose` and `onSave`.

These can be called by using the following code:

```typescript
const modal = this.modalService.createModal(FancyModalComponent);

modal.onClose.then(() => {
  console.log("Modal closed");
});

modal.onSave.then((data) => {
  console.log(data);
});
```
