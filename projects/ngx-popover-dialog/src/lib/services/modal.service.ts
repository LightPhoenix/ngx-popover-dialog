import { PopoverDialogRef, PopoverDialog } from './../classes/popover-dialog';
import { Injectable, Injector, HostListener } from '@angular/core';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { OverlayTokenData, OVERLAY_TOKEN } from '../contracts/overlay';
import { PortalInjector, ComponentPortal, ComponentType } from '@angular/cdk/portal';
import { Modal } from '../classes';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  private modalList: Array<PopoverDialogRef> = [];

  constructor(private injector: Injector, private overlay: Overlay) { }

  private createInjector(overlayRef: OverlayRef, data: OverlayTokenData): [PortalInjector, PopoverDialogRef] {
    const injectionTokens = new WeakMap();

    const dialog = new PopoverDialog();

    injectionTokens.set(OVERLAY_TOKEN, {
      overlayRef,
      data,
      dialog
    });

    const popoverDialogRef: PopoverDialogRef = {
      onClose: dialog.onClose(),
      onSave: dialog.onSave(),
      overlayRef
    };

    return [new PortalInjector(this.injector, injectionTokens), popoverDialogRef];
  }

  createModal(component: ComponentType<Modal>, data?: OverlayTokenData): PopoverDialogRef {
    const overlayRef = this.overlay.create({ hasBackdrop: true });
    const [injector, popoverDialogRef] = this.createInjector(overlayRef, data || {});
    const portal = new ComponentPortal(component, null, injector);
    overlayRef.attach(portal);

    this.modalList.push(popoverDialogRef);

    return popoverDialogRef;
  }

  closeLastModal(): void {
    if (this.modalList.length > 0) {
      /* Close the last modal dialog */
      this.modalList[this.modalList.length - 1].overlayRef.dispose();

      /* Remove the modal from the list */
      this.modalList.splice(-1, 1);
    }
  }

}
