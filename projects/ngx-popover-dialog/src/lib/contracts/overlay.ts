import { PopoverDialogRef, PopoverDialog } from './../classes/popover-dialog';
import { InjectionToken } from '@angular/core';
import { OverlayRef } from '@angular/cdk/overlay';

export const OVERLAY_TOKEN = new InjectionToken<OverlayToken>('OVERLAY_TOKEN_DATA');

export interface OverlayToken {
    overlayRef: OverlayRef;
    data: OverlayTokenData;
    dialog: PopoverDialog;
}

export interface OverlayTokenData {
    [key: string]: any;
}
