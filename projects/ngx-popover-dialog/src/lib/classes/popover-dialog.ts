import { Subject } from 'rxjs';
import { OverlayRef } from '@angular/cdk/overlay';

export class PopoverDialog {

    private onCloseObservable = new Subject<void>();
    private onSaveObservable = new Subject<any>();

    onClose(): Promise<void> {
        return this.onCloseObservable.toPromise();
    }

    close(): void {
        this.onCloseObservable.next();
        this.onCloseObservable.complete();
    }

    onSave(): Promise<any> {
        return this.onSaveObservable.toPromise();
    }

    save(value: any): void {
        this.onSaveObservable.next(value);
        this.onSaveObservable.complete();
    }

}

export interface PopoverDialogRef {
    onClose: Promise<void>;
    onSave: Promise<any>;
    overlayRef: OverlayRef;
}
