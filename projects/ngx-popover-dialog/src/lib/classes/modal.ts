import { ModalService } from './../services/modal.service';
import { Inject, HostListener } from '@angular/core';
import { OVERLAY_TOKEN, OverlayToken } from '../contracts/overlay';

export abstract class Modal {

    /* Dispose/close on esc key */
    @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent): void {
        this.modalService.closeLastModal();
    }

    constructor(@Inject(OVERLAY_TOKEN) protected overlay: OverlayToken, protected modalService: ModalService) { }

    close(): void {
        /* Dispose the overlayref */
        this.overlay.overlayRef.dispose();

        /* Tell the obserable that the dialog will be closed*/
        this.overlay.dialog.close();
    }

    save(data: any): void {
        /* Dispose the overlayref */
        this.overlay.overlayRef.dispose();

        /* Tell the observalbe that the dialog is saved and pass the data */
        this.overlay.dialog.save(data);
    }

}
